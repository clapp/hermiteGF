% This is  This is a test of the anisotropic HermiteGF method with x0 = (0, 0) for unit square domain
clear
%close all

% Add necessary paths
addpath('../HermiteGF_QR')
addpath('../HermiteGF_QR/nodes_generation')
addpath('../HermiteGF_QR/HermiteGF_expansion')
addpath('../HermiteGF_QR/helper_functions')

% Choose a test function
func = 'anisotropic';

% Choose whether to compare to CQR, GQR or Direct methods
benchmark_cheb =  false; %true; %
benchmark_direct = false; %true; %
benchmark_gaussQR = false; %true; % 

% Choose the test type
test = 'pEps'; %'pN'; % 

% Set up the method
tvec = linspace(0.3, 0.99, 10); % the possible values of t
TOL = 1e-6; % the tolerance for the cut-off criterion
maxlength = inf; % Maximum available amount of basis functions
Pg = [1 0.3; 0.1 1.3]; % Pattern for the G matrix

% Interpolated function
switch func
    case 'anisotropic'
        f = @(x, y) 1./((x.^2 + x.*y + y.^2) + 2);
end

% Set up the square domain
Ne = [53, 53];

% Generate evaluation points
xe = generate_square_nodes('Uniform', false, Ne);
Ne = size(xe, 1);

% Setup node generation
clustering = true;
generate_nodes = @(N) generate_square_nodes('Halton', clustering, N);

% Setup error computation
fvals = f(xe(:,1), xe(:,2));
compute_error = @(interpolant) compute_avg_error(interpolant, fvals);


switch (test)
    case 'pEps'
        % Test the dependency of the error and conditioning from the values
        % of the scaling p of the off-diagonal elements of E and the value of epsilon. 
        % Corresponds to Fig. 7b
        Nvec = 130; % value of N
        gammaval = 3.5; % value of gamma
        pvec = 0:0.05:0.8; % values of p
        epvec = logspace(-3, 0.1, 30); % values of epsilon
    case 'pN'
        % Test the dependency of the error and conditioning from the values
        % of the scaling p of the off-diagonal elements of E and the number of
        % collocation points N Corresponds to Fig. 7a
        Nvec = 50:10:340; % values of N
        gammaval = 3.5; % value of gamma
        epvec = 0.03; % value of epsilon
        pvec = 0:0.05:0.8; % values of p
end

if (benchmark_direct)
    % Initialize the arrays for observables
    error_d = zeros(numel(epvec), numel(Nvec), numel(gammavec));
    cond_d = zeros(numel(epvec), numel(Nvec), numel(gammavec));
end

% Initialize the arrays for observables
error_h = zeros(numel(epvec), numel(Nvec), numel(pvec));
cond_h = zeros(numel(epvec), numel(Nvec), numel(pvec));

% Set the matrix G
G = gammaval.*Pg;
for p_it = 1:numel(pvec) % loop through the values of p
    
    % Form the pattern for the shape matrix E
    Pe = [1 pvec(p_it); pvec(p_it) 1];
    
    for ep_it=1:numel(epvec) % loop through the values of epsilon
        
        % Form the matrix E
        ep = epvec(ep_it);
        E = Pe.*ep;
        E = E'*E;
        
        for N_it = 1:numel(Nvec) % loop through the values of N
            N = Nvec(N_it);
            
            % Compute the highest polynomial degree corresponding to the first
            % N basis functions
            K = compute_K(N, 2);
            
            % Generate collocation points
            xk = generate_nodes(N);
            
            % Evaluate the inteprolated function at collocation points
            y = f(xk(:, 1), xk(:, 2));
            
            % Compute the HermiteGF-QR interpolant
            [s, cond_A0, jmax] = compute_QR_interpolant(E, G, xk, xe, K, maxlength, y, tvec, TOL);
            
            % Save the conditioning of the collocation matrix
            cond_h(ep_it, N_it, p_it) = cond_A0;
            
            % Compute the error
            error_h(ep_it, N_it, p_it) = compute_error(s);
            
            if benchmark_direct
                % Compute the RBF-Direct interpolant
                [cond_d(ep_it, N_it, p_it), interpolation_matrix_dir] = compute_interp_mtx_direct(ep, xk, xe, Pe);
                s_dir_a = interpolation_matrix_dir*f(xk(:, 1), xk(:, 2));
                
                % Compute the error
                error_d(ep_it, N_it, p_it) = compute_error(s_dir_a);
            end
            
        end
        
    end
end

% The size of the figures window
x0=10;
y0=10;
height = 400;
width=500;

% Plotting of the results for all test cases
switch (test)
    case 'pEps'
        [xx yy] = ndgrid(epvec, pvec);
        figure(1)
        error_plot = reshape(error_h, size(xx));
        surf(log10(xx), yy, log10(error_plot));
        axis([min(log10(epvec)) max(log10(epvec)) min(pvec) max(pvec) min(min(log10(error_h)))-2 max(max(log10(error_h)))+2])
        h = colorbar;
        set(get(h,'title'),'string','error', 'Interpreter', 'Latex', 'Fontsize', 18);
        xticklabels(generate_log_labels(xticks));
        colormap jet
        set(gcf,'units','points','position',[x0,y0,width,height])
        set(gca, 'Fontsize', 18)
        xlabel('$\varepsilon$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('$p$', 'Interpreter', 'Latex', 'Fontsize', 18)
        title(strcat('$\gamma=', num2str(gammaval),', N=', num2str(Nvec),'$'), 'Interpreter', 'Latex', 'Fontsize', 18);
        set_log_ticklabels_colorbar(h);
        tstart = strsplit(num2str(min(tvec)), '.');
        filename = strcat('figures/pEps_t_from', tstart{1}, tstart{2}, '_error.fig');
        savefig(filename)
        
        figure(2)
        cond_plot = reshape(cond_h, size(xx));
        surf(log10(xx), yy, log10(cond_plot))
        colormap jet
        colorbar
        axis([min(log10(epvec)) max(log10(epvec)) min(pvec) max(pvec) min(min(log10(cond_h)))-2 max(max(log10(cond_h)))+2])
        h = colorbar;
        set(get(h,'title'), 'string', 'cond', 'Interpreter', 'Latex', 'Fontsize', 18);
        xticklabels(generate_log_labels(xticks));
        set(gcf,'units','points','position',[x0,y0,width,height])
        set(gca, 'Fontsize', 18)
        xlabel('$\varepsilon$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('$p$', 'Interpreter', 'Latex', 'Fontsize', 18)
        title(strcat('$\gamma=', num2str(gammaval),', N=', num2str(Nvec), ', t \in (', num2str(min(tvec)), ',', num2str(ceil(max(tvec))), ')$'), 'Interpreter', 'Latex', 'Fontsize', 18);
        set_log_ticklabels_colorbar(h);
        filename = strcat('figures/pEps_t_from', tstart{1}, tstart{2}, '_cond.fig');
        savefig(filename)
        
    case 'pN'
        [xx yy] = ndgrid(Nvec, pvec);
        figure(1)
        error_plot = reshape(error_h, size(xx));
        surf(xx, yy, log10(error_plot));
        axis([min(Nvec) max(Nvec) min(pvec) max(pvec) min(min(log10(error_h)))-2 max(max(log10(error_h)))+2])
        h = colorbar;
        set(get(h,'title'),'string','error', 'Interpreter', 'Latex', 'Fontsize', 18);
        colormap jet
        set(gcf,'units','points','position',[x0,y0,width,height])
        set(gca, 'Fontsize', 18)
        xlabel('$N$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('$p$', 'Interpreter', 'Latex', 'Fontsize', 18)
        title(strcat('$\gamma=', num2str(gammaval),', \,\varepsilon=', num2str(epvec), ', t \in (', num2str(min(tvec)), ',', num2str(ceil(max(tvec))), ')$'), 'Interpreter', 'Latex', 'Fontsize', 18);
        set_log_ticklabels_colorbar(h);
        tstart = strsplit(num2str(min(tvec)), '.');
        filename = strcat('figures/pN_t_from', tstart{1}, tstart{2}, '_error.fig');
        savefig(filename)
        
        
        figure(2)
        cond_plot = reshape(cond_h, size(xx));
        surf(xx, yy, log10(cond_plot))
        colormap jet
        colorbar
        axis([min(Nvec) max(Nvec) min(pvec) max(pvec) min(min(log10(cond_h)))-2 max(max(log10(cond_h)))+2])
        h = colorbar;
        set(get(h,'title'),'string','cond', 'Interpreter', 'Latex', 'Fontsize', 18);
        set(gcf,'units','points','position',[x0,y0,width,height])
        set(gca, 'Fontsize', 18)
        xlabel('$N$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('$p$', 'Interpreter', 'Latex', 'Fontsize', 18)
        title(strcat('$\gamma=', num2str(gammaval),', \,\varepsilon=', num2str(epvec), ', t \in (', num2str(min(tvec)), ',', num2str(ceil(max(tvec))), ')$'), 'Interpreter', 'Latex', 'Fontsize', 18);
        set_log_ticklabels_colorbar(h);
        filename = strcat('figures/pN_t_from', tstart{1}, tstart{2}, '_cond.fig');
        savefig(filename)
end