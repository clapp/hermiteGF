% This is a test of the isotropic HermiteGF method with x0 = (0, 0) for hyperbolic domain
clear
%close all

% Add necessary paths
addpath('../HermiteGF_QR')
addpath('../HermiteGF_QR/nodes_generation')
addpath('../HermiteGF_QR/HermiteGF_expansion')
addpath('../HermiteGF_QR/helper_functions')

% Chose a test function
func = 'hypDomainTestFunc';

% Choose whether to compare to CQR, GQR or Direct methods
benchmark_cqr =  false; %true; %
benchmark_gqr = false; %true; %
benchmark_direct = false; %true; %

% Choose the test type
test = 'varyNgood'; %'gammaN'; %'varyNall'; %

% Set up the method
tvec = linspace(0.3, 0.99, 10); % the possible values of t
TOL = 1e-6; % the tolerance for the cut-off criterion
maxlength = inf; %N+200; % Maximum available amount of basis functions

% Interpolated function
switch func
    case 'hypDomainTestFunc'
        f = @(x, y) sin((x.^2 + 2*y.^2)) - sin((2*x.^2 + (y - 0.5).^2));
end

% Set up the domain
load('xe_circle_c02_1.mat') % load the evaluation points
Ne = size(xe, 1);

% Setup node generation
generate_nodes = @(N) generate_hyperbolic_nodes(N);

% Setup error computation
fvals = f(xe(:,1), xe(:,2));
compute_error = @(interpolant) compute_avg_error(interpolant, fvals);

% Choose a test
switch (test)
    case 'varyNgood' 
        % Test the dependency of the error and conditioning from N for
        % "good" values of N corresponding so that there are no same powers
        % epsilon present in D_1 and D_2. Corresponds to the Fig. 4
        Nvec =  [105 120 136 153 171 190 210 231 253 276 300 325 351 378 406]; %"good" values of N
        epvec = 0.05; % value of epsilon
        gammavec = 3.5; % value of gamma
    case 'varyNall'
        % Test the dependency of the error and conditioning from N for all
        % N from the interval [300:351]. Corresponds to the "zoomed
        % regions" in Fig. 4
        Nvec =  300:351; % values of N
        epvec = 0.05; % value of epsilon
        gammavec = 3.5; % value of gamma
    case 'gammaN'
        % Test the dependency of the error and conditioning from the values
        % of gamma and N. Corresponds to Fig. 5
        gammavec = 1:0.5:8; % values of gamma
        Nvec = 100:20:406; % values of N
        epvec = 0.05; % value of epsilon
end

if  (benchmark_cqr)
    % Modify the setup to work with the [0, 1] interval of the Chebyshev-QR
    % code
    xe_c = 0.5*xe + 0.5;
    f_c = @(x, y) f(2.*x - 1, 2.*y - 1);
    epvec_c = epvec*2;
    
    % Initialize the arrays for observables
    error_c = zeros(numel(epvec), numel(Nvec), numel(gammavec));
    cond_c = zeros(numel(epvec), numel(Nvec), numel(gammavec));
end

if (benchmark_gqr)
    % Initialize the arrays for observables
    error_g = zeros(numel(epvec), numel(Nvec), numel(gammavec));
    cond_g = zeros(numel(epvec), numel(Nvec), numel(gammavec));
end

if (benchmark_direct)
    % Initialize the arrays for observables
    error_d = zeros(numel(epvec), numel(Nvec), numel(gammavec));
    cond_d = zeros(numel(epvec), numel(Nvec), numel(gammavec));
end

% Initialize the arrays for observables
error_h = zeros(numel(epvec), numel(Nvec), numel(gammavec));
cond_h = zeros(numel(epvec), numel(Nvec), numel(gammavec));

for ep_it=1:numel(epvec) % loop through the values of epsilon
    
    % Form the E matrix
    ep = epvec(ep_it);
    E = eye(2)*ep; % in the isotropic case matrix E is diagonal
    E = E'*E;
    
    for N_it = 1:numel(Nvec) % loop through the values of N
        N = Nvec(N_it);
        
        % Compute the highest polynomial degree corresponding to the first
        % N basis functions
        K = compute_K(N, 2);
        
        % Generate collocation points
        xk = generate_nodes(N);
        
        % Evaluate the inteprolated function at collocation points
        y = f(xk(:, 1), xk(:, 2));
        
        for gamma_it = 1:numel(gammavec) % loop through the values of gamma
            
            % Form the G matrix
            gamma = gammavec(gamma_it);
            G = gamma.*eye(2);
            
            % Compute the HermiteGF-QR interpolant
            [s, cond_A0, jmax] = compute_QR_interpolant(E, G, xk, xe, K, maxlength, y, tvec, TOL);
            
            % Save the conditioning of the collocation matrix
            cond_h(ep_it, N_it, gamma_it) = cond_A0;
            
            % Compute the error
            error_h(ep_it, N_it, gamma_it) = compute_error(s);
            
            if benchmark_gqr % if we want to compare to the result of the GaussQR method
                if (exist('gaussqr') == 7)
                    % Compute the GaussQR interpolant
                    GQR = gqr_solve(xk,y,ep,gamma);
                    yp = gqr_eval(GQR,xe);
                    
                    % Compute the observables
                    error_g(ep_it, N_it, gamma_it) = compute_error(yp);
                    cond_g(ep_it, N_it, gamma_it) = cond(GQR.stored_psi);
                    if (norm(GQR.A - GQR.stored_psi) ~= 0)
                        disp('alarm!')
                    end
                else
                    fprintf('Please download the gaussQR library and run "rbfsetup.m" in its main folder in order to use the "benchmark_gqr" option.\n');
                    return
                end
            end
            
            if benchmark_cqr % if we want to compare to the result of the Chebyshev-QR method
                if (exist('RBF_QR_diffmain_2D.m') == 2)
                    % Scale xk to the interval [0, 1] interval of the Chebyshev-QR code
                    xk_c = 0.5*xk + 0.5;
                    
                    % Compute Chebyshev-QR interpolant
                    [A,Psi] = RBF_QR_diffmat_2D('1',xe_c,xk_c,epvec_c(ep_it));
                    s_c = A*f_c(xk_c(:, 1), xk_c(:, 2));
                    
                    % Compute the observables
                    error_c(ep_it, N_it, gamma_it) = compute_error(s_c);
                    cond_c(ep_it, N_it, gamma_it) = cond(Psi.A0);
                else
                    fprintf('Please download the RBF-QR code with Chebyshev polynomials and add it to the MATLAB path. The "benchmark_cqr" option is available only if the RBF-QR code is installed.\n');
                    return
                end
            end
            
            if benchmark_direct
                % Compute the RBF-Direct interpolant
                [cond_d(ep_it, N_it, gamma_it), interpolation_matrix_d] = compute_interp_mtx_direct(ep, xk, xe, eye(2));
                s_d = interpolation_matrix_d*f(xk(:, 1), xk(:, 2));
                
                % Compute the error
                error_d(ep_it, N_it, gamma_it) = compute_error(s_d);
            end
            
        end
    end
end

% The size of the figures window
x0=10;
y0=10;
height = 400;
width=500;

% Plotting of the results for all test cases
switch (test)
    case 'varyNall' 
        figure(1)
        a2 = axes();
        a2.Position = [0.3200 0.6600 0.4 0.25]; % xlocation, ylocation, xsize, ysize
        semilogy(a2, Nvec, error_h, '-', 'Linewidth', 1, 'Color', [0 0.7 0]);
        hold on
        semilogy([300 325 351], error_h([1 26 end]), 'LineStyle', 'none', 'Linewidth', 1.5, 'Marker', '<', 'MarkerSize', 8,'Color', [0 0.7 0]);
        minval = min(error_h);
        maxval = max(error_h);
        
        if (benchmark_gqr)
            semilogy(a2, Nvec, error_g, 'Linewidth', 1,  'Color', [0 0 0.7]);
            semilogy([300 325 351], error_g([1 26 end]), 'LineStyle', 'none', 'Linewidth', 1.5, 'Marker', 'o', 'MarkerSize', 8,'Color', [0 0 0.7]);
            minval = min([minval, error_g]);
            maxval = max([maxval, error_g]);
        end
        
        if (benchmark_cqr)
            semilogy(a2, Nvec, error_c, '-', 'Linewidth', 1, 'Color', [0.4 0.4 0.4]);
            semilogy([300 325 351], error_c([1 26 end]), 'LineStyle', 'none', 'Linewidth', 1.5, 'Marker', 's', 'MarkerSize', 8,'Color', [0.4 0.4 0.4]);
            minval = min([minval, error_c]);
            maxval = max([maxval, error_c]);
        end

        set(gca, 'Fontsize', 12)
        axis(a2, [min(Nvec) max(Nvec) 0.8*minval 2*maxval])
        
        figure(2)
        a2 = axes();
        a2.Position = [0.3200 0.6600 0.4 0.25]; % xlocation, ylocation, xsize, ysize
        semilogy(a2, Nvec, cond_h, '-', 'Linewidth', 1, 'Color', [0 0.7 0])
        hold on
        semilogy([300 325 351], cond_h([1 26 end]), 'LineStyle', 'none', 'Linewidth', 1.5, 'Marker', '<', 'MarkerSize', 8, 'Color', [0 0.7 0]);
        minval = min(cond_h);
        maxval = max(cond_h);
        if (benchmark_gqr)
            semilogy(a2, Nvec, cond_g, '-', 'Linewidth', 1, 'Color', [0 0 0.7])
            hold on
            semilogy([300 325 351], cond_g([1 26 end]), 'LineStyle', 'none', 'Linewidth', 1.5, 'Marker', 'o', 'MarkerSize', 8, 'Color', [0 0 0.7]);
            minval = min([minval, cond_g]);
            maxval = max([maxval, cond_g]);
        end
        
        if (benchmark_cqr)
            semilogy(a2, Nvec, cond_c, '-','Linewidth', 1, 'Color', [0.4 0.4 0.4])
            hold on
            semilogy([300 325 351], cond_c([1 26 end]), 'LineStyle', 'none', 'Linewidth', 1.5, 'Marker', 's', 'MarkerSize', 8, 'Color', [0.4 0.4 0.4]);
            minval = min([minval, cond_c]);
            maxval = max([maxval, cond_c]);
        end
        set(gca, 'Fontsize', 12)
        axis(a2, [min(Nvec) max(Nvec) minval*1e-1 maxval*1e2])
        
    case 'varyNgood'
        figure(1)
        leg1 = legend;
        l1 = leg1.String;
        l1{end+1} = strcat('HGFQR', ', $t \in (', num2str(min(tvec)), ',', num2str(ceil(max(tvec))), ')$');
        semilogy(Nvec, error_h, '-.', 'Linewidth', 1.5, 'Marker', '<', 'MarkerSize', 8, 'Color', [0 0.7 0]);
        hold on
        minval = min(error_h);
        maxval = max(error_h);
        if (benchmark_direct)
            semilogy(Nvec, error_d, ':', 'Linewidth', 1.5, 'Marker', '*', 'MarkerSize', 8, 'Color', [0.7 0 0]);
            l1{end+1} = 'Direct';
            minval = min([minval, error_d]);
            maxval = max([maxval, error_d]);
        end
        if (benchmark_cqr)
            semilogy(Nvec, error_c, '-', 'Linewidth', 1.5, 'Marker', 's', 'MarkerSize', 8, 'Color', [0.4 0.4 0.4]);
            l1{end+1} = 'CQR';
            minval = min([minval, error_c]);
            maxval = max([maxval, error_c]);
        end
        if (benchmark_gqr)
            semilogy(Nvec, error_g, 'Linewidth', 1.5, 'Marker', 'o', 'MarkerSize', 8, 'Color', [0 0 0.7])
            l1{end+1} = 'GQR';
            minval = min([minval, error_g]);
            maxval = max([maxval, error_g]);
        end
        set(gcf,'units','points','position',[x0,y0,width,height])
        set(gca, 'Fontsize', 18)
        title(strcat('$\gamma = ', num2str(gammavec),'$, $\varepsilon = ', num2str(epvec),'$'), 'FontSize', 18, 'Interpreter', 'Latex');
        legend(l1, 'Interpreter', 'Latex', 'Fontsize', 18);
        
        axis([min(Nvec)-3 max(Nvec)+3 0.8*minval 2*maxval])
        xlabel('$N$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('Average error', 'Interpreter', 'Latex', 'Fontsize', 18)
        hold on
        
        figure(2)
        leg2 = legend;
        l2 = leg2.String;
        l2{end+1} = strcat('HGFQR', ', $t \in (', num2str(min(tvec)), ',', num2str(ceil(max(tvec))), ')$');
        semilogy(Nvec, cond_h, '-.', 'Linewidth', 1.5, 'Marker', '<','MarkerSize', 8,  'Color', [0 0.7 0]);
        hold on
        minval = min(cond_h);
        maxval = max(cond_h);
        if (benchmark_direct)
            semilogy(Nvec, cond_d, ':', 'Linewidth', 1.5, 'Marker', '*', 'MarkerSize', 8, 'Color', [0.7 0 0]);
            l2{end+1} = 'Direct';
            minval = min([minval, cond_d]);
            maxval = max([maxval, cond_d]);
        end
        if (benchmark_cqr)
            semilogy(Nvec, cond_c, '-', 'Linewidth', 1.5, 'Marker', 's', 'MarkerSize', 8, 'Color', [0.4 0.4 0.4]);
            l2{end+1} = 'CQR';
            minval = min([minval, cond_c]);
            maxval = max([maxval, cond_c]);
        end
        if (benchmark_gqr)
            semilogy(Nvec, cond_g, 'Linewidth', 1.5, 'Marker', 'o','MarkerSize', 8,  'Color', [0 0 0.7])
            l2{end+1} = 'GQR';
            minval = min([minval, cond_g]);
            maxval = max([maxval, cond_g]);
        end
        set(gcf,'units','points','position',[x0,y0,width,height])
        set(gca, 'Fontsize', 18)
        title(strcat('$\gamma = ', num2str(gammavec),'$, $\varepsilon = ', num2str(epvec),'$'), 'FontSize', 18, 'Interpreter', 'Latex');
        legend(l2, 'Interpreter', 'Latex', 'Fontsize', 18, 'Location', 'SouthEast');
        
        axis([min(Nvec)-3 max(Nvec)+3 minval*1e-1 maxval*1e2])
        xlabel('$N$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('Condition number', 'Interpreter', 'Latex', 'Fontsize', 18)

    case 'gammaN'
        [xx yy] = ndgrid(Nvec, gammavec);
        figure(1)
        error_plot = reshape(error_h, size(xx));
        surf(xx, yy, log10(error_plot));
        axis([min(Nvec) max(Nvec) min(gammavec) max(gammavec) min(log10(error_h(:)))-1 max(log10(error_h(:)))+1])
        h = colorbar;
        set(get(h,'title'),'string','error', 'Interpreter', 'Latex', 'Fontsize', 18);
        colormap jet
        set(gcf,'units','points','position',[x0,y0,width,height])
        set(gca, 'Fontsize', 18)
        xlabel('$N$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('$\gamma$', 'Interpreter', 'Latex', 'Fontsize', 18)
        title(strcat('$\varepsilon = ', num2str(epvec), ', t \in (', num2str(min(tvec)), ',', num2str(ceil(max(tvec))), ')$'), 'Interpreter', 'Latex', 'Fontsize', 18)
        set_log_ticklabels_colorbar(h);
        tstart = strsplit(num2str(min(tvec)), '.');
        filename = strcat('figures/gammaN_t_from', tstart{1}, tstart{2}, '_eps', num2str(epvec),'_error.fig');
        savefig(filename)
        
        figure(2)
        cond_plot = reshape(cond_h, size(xx));
        surf(xx, yy, log10(cond_plot));
        axis([min(Nvec) max(Nvec) min(gammavec) max(gammavec) min(log10(cond_h(:)))-1 max(log10(cond_h(:)))+1])
        h = colorbar;
        set(get(h,'title'),'string','cond', 'Interpreter', 'Latex', 'Fontsize', 18);
        colormap jet
        set(gcf,'units','points','position',[x0,y0,width,height])
        set(gca, 'Fontsize', 18)
        xlabel('$N$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('$\gamma$', 'Interpreter', 'Latex', 'Fontsize', 18)
        title(strcat('$\varepsilon = ', num2str(epvec), ', t \in (', num2str(min(tvec)), ',', num2str(ceil(max(tvec))), ')$'), 'Interpreter', 'Latex', 'Fontsize', 18)
        set_log_ticklabels_colorbar(h);
        filename = strcat('figures/gammaN_t_from', tstart{1}, tstart{2}, '_eps', num2str(epvec), '_cond.fig');
        savefig(filename)
end