% This a test of comparing the values of jmax for isotropic HermiteGF method 
% with x0 = (0, 0) with reference methods
clear
%close all

% Add necessary paths
addpath('../HermiteGF_QR')
addpath('../HermiteGF_QR/nodes_generation')
addpath('../HermiteGF_QR/HermiteGF_expansion')
addpath('../HermiteGF_QR/helper_functions')

global GAUSSQR_PARAMETERS % necessary for comparisson with the GaussQR code

% Chose a test function
fun = 'hypDomainTestFunc';

% Choose whether to compare to CQR or GQR
benchmark_cqr = false; %true; %
benchmark_gqr = false; %true; %

% Set up the method
tvec = linspace(0.1, 0.99, 30); % the possible values of t
TOL = 1e-2; % the tolerance for the cut-off criterion
maxlength = inf; %N+200; % Maximum available amount of basis functions

% Interpolated function
switch fun
    case 'hypDomainTestFunc'
        f = @(x, y) sin((x.^2 + 2*y.^2)) - sin((2*x.^2 + (y - 0.5).^2));
end

% Set up the domain
load('xe_circle_c02_1.mat') % load the evaluation points
Ne = size(xe, 1);

% Setup node generation
generate_nodes = @(N) generate_hyperbolic_nodes(N);

% Setup error computation
fvals = f(xe(:,1), xe(:,2));
compute_error = @(interpolant) compute_avg_error(interpolant, fvals);

% Fixed parameters
N = 110; % number of collocation points
gammaval = 3.5; % value of gamma
G = eye(2).*gammaval; % matrix G
Pe = eye(2); % the pattern for the shape matrix

% Set range for the values of epsilon
epvec = logspace(-3, 0.1, 30);

if benchmark_cqr
    % Modify the setup to work with the [0, 1] interval of the Chebyshev-QR
    % code
    xe_c = 0.5*xe + 0.5;
    f_c = @(x, y) f(2.*x - 1, 2.*y - 1);
    epvec_c = epvec*2;
    
    % Initialize the arrays for observables
    error_c = zeros(size(epvec)); % error
    j_c = zeros(size(epvec)); % jmax
end

if benchmark_gqr
    % Initialize the arrays for observables
    error_g = zeros(size(epvec)); % error
    j_g = zeros(size(epvec)); % jmax
end

% Compute the highest polynomial degree corresponding to the first
% N basis functions
K = compute_K(N, 2);

% Generate collocation nodes
xk = generate_nodes(N);

% Evaluate the inteprolated function at collocation points
y = f(xk(:, 1), xk(:, 2));

% Initialize the arrays for observables
error_h = zeros(size(epvec)); % error
j_h = zeros(size(epvec)); % jmax
for ep_it=1:numel(epvec)
    
    % Form the E matrix
    ep = epvec(ep_it);
    E = Pe.*ep;
    E = E'*E;
    
    % Compute the HermiteGF-QR interpolant
    [s, cond_A0, jmax] = compute_QR_interpolant(E, G, xk, xe, K, maxlength, y, tvec, TOL);
    
    % Compute the errors
    error_h(ep_it) = compute_error(s);
    
    % Save the jmax
    j_h(ep_it) = jmax;
        
    if benchmark_gqr
        if (exist('gaussqr') == 7)
            GAUSSQR_PARAMETERS.MAX_EXTRA_EFUNC = Inf;
            
            % Compute the GaussQR interpolant
            GQR = gqr_solve(xk,y,ep,gammaval);
            yp = gqr_eval(GQR,xe);
            
            % Save the observables
            j_g(ep_it) = max(sum(GQR.Marr-1, 1));
            error_g(ep_it) = compute_error(yp);
        else
            fprintf('Please download the gaussQR library and run "rbfsetup.m" in its main folder in order to use the "benchmark_gqr" option.\n');
            return
        end
    end
    
    if benchmark_cqr
        if (exist('RBF_QR_diffmain_2D.m') == 2)
            % Scale xk to the interval [0, 1] interval of the Chebyshev-QR code
            xk_c = 0.5*xk + 0.5;

            % Compute Chebyshev-QR interpolant
            [A,Psi] = RBF_QR_diffmat_2D('1',xe_c,xk_c,epvec_c(ep_it));
            j_c(ep_it) = compute_K(N + size(Psi.Rt, 2), 2);
            s_c = A*f_c(xk_c(:, 1), xk_c(:, 2));
            
            % Compute the observables
            error_c(ep_it) = compute_error(s_c);
            cond_c(ep_it) = cond(Psi.A0);
        else
            fprintf('Please download the RBF-QR code with Chebyshev polynomials and add it to the MATLAB path. The "benchmark_cqr" option is available only if the RBF-QR code is installed.\n');
            return
        end
    end
    
end


% The size of the figures window
x0=10;
y0=10;
height = 400;
width=500;

figure(1)
leg1 = legend;
l1 = leg1.String;
l1{end+1} = strcat('HGFQR');
semilogx(epvec, j_h, '-.','Linewidth', 1.5, 'Color', [0 0.7 0], 'Linewidth', 1.5, 'Marker', 's', 'MarkerSize', 9,'MarkerIndices', 1:3:length(error_h));
minval = min(j_h);
maxval = max(j_h);
hold on
if (benchmark_cqr)
    semilogx(epvec, j_c, '--', 'Linewidth', 1.5, 'Marker', 's', 'MarkerSize', 9, 'Color', [0.4 0.4 0.4], 'MarkerIndices', 3:3:length(error_c))
    l1{end+1} = 'CQR';
    minval = min([minval, j_c]);
    maxval = max([maxval, j_c]);
end
if (benchmark_gqr)
    semilogx(epvec, j_g ,':', 'Linewidth', 1.5, 'Marker', 'o', 'MarkerSize', 9,'Color', [0 0 0.7], 'MarkerIndices', 3:3:length(error_g))
    l1{end+1} = 'GQR';
    minval = min([minval, j_g]);
    maxval = max([maxval, j_g]);
end

xlabel('$\varepsilon$', 'Interpreter', 'Latex', 'Fontsize', 18)
ylabel('$j_{\mathrm{max}}$', 'Interpreter', 'Latex', 'Fontsize', 18)
legend(l1, 'Interpreter', 'Latex', 'Fontsize', 18, 'Location', 'SouthEast')
set(gca, 'Fontsize', 18)
axis([min(epvec) max(epvec) (minval - 2) (maxval + 2)])
title(strcat(strcat('$\gamma=', num2str(gammaval), ',\, N = ', num2str(N), '$')), 'Interpreter', 'Latex', 'Fontsize', 18)
set(gcf, 'units', 'points', 'position', [x0,y0,width,height])

figure(2)
leg2 = legend;
l2 = leg2.String;
if (benchmark_gqr)  
    loglog(epvec, abs(error_h - error_g),'-.', 'Linewidth', 1.5, 'Marker', 's', 'MarkerSize', 9,'Color', [0 0.7 0], 'MarkerIndices', 2:3:length(error_g))    
    hold on
    error_diff = abs(error_h - error_g);
    l2{end+1} = 'HGFQR';
    if (benchmark_cqr)
        loglog(epvec, abs(error_c - error_g),':', 'Linewidth', 1.5, 'Marker', '>', 'MarkerSize', 9,'Color', [0.4 0.4 0.4], 'MarkerIndices', 2:3:length(error_g))
        l2{end+1} = 'CQR vs GQR';
    end
    
end
if ( (benchmark_cqr) || (benchmark_gqr) )
    set(gcf,'units','points','position',[x0,y0,width,height])
    xlabel('$\varepsilon$', 'Interpreter', 'Latex', 'Fontsize', 18)
    ylabel('Error difference', 'Interpreter', 'Latex', 'Fontsize', 18)
    legend(l2, 'Interpreter', 'Latex', 'Fontsize', 18, 'Location', 'SouthEast')
    set(gca, 'Fontsize', 18)
    minval = min(error_diff);
    maxval = max(error_diff);
    axis([min(epvec) max(epvec) 0.8*minval 2*maxval])
    title(strcat(strcat('$\gamma=', num2str(gammaval), ',\, N = ', num2str(N), '$')), 'Interpreter', 'Latex', 'Fontsize', 18)
    set(gcf,'units','points','position',[x0,y0,width,height])
end