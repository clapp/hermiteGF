% This is a general HermiteGF-QR code for any dimension. However, only
% for simple tests on a UNIT DOMAIN with Halton points for
% both collocation and evaluation grids and with x0 = (0, 0) shift of the basis functions.
clear
%close all

% Add necessary paths
addpath('../HermiteGF_QR')
addpath('../HermiteGF_QR/nodes_generation')
addpath('../HermiteGF_QR/HermiteGF_expansion')
addpath('../HermiteGF_QR/helper_functions')
global GAUSSQR_PARAMETERS % necessary for comparisson with the GaussQR code

% Setup the parameters
benchmark_direct = false; %true; %
benchmark_gqr = false; %true; %

% Choose the test type
test = 'ndtest'; %'3dtest'; %

switch test
    case '3dtest'
        % Test the dependency of the error from the value of epsilon for an
        % anisotropic 3d interpolation
        d = 3; % dimensionality
        % Choose a pattern for the shape matrix. Identity yields the isotropic case
        Pe = [1 0.2 0.3; 0.2 1 0.15; 0.1 0.3 1];
        Nvec = 125; %250; % number of basis functions
    case 'ndtest'
        % Test the dependency of the error from the value of epsilon for an
        % isotropic 3-5d interpolation
        d = 3; % dimensionality
        % Choose a pattern for the shape matrix. Identity yields the isotropic case
        Pe = eye(d);
        % Number of collocation points
        Nvec = 4^d;
end

% Test function
f = @(x)(cos(sum(x, 2)));

% Values of epsilon
epvec = logspace(-3, 0.1, 30); 

% Number of evaluation points
Ne = Nvec + 1000;

maxlength = Inf; % the maximum allowed number of basis functions

% Value of gamma
gammaval = 5;

% Choose a pattern for gamma. For now, multidimensional tests have been
% carried out only with the identity.
Pg = eye(d);
G = gammaval.*Pg;

% The shift of the basis. FOR NOW WORKS ONLY WITH ZERO SHIFT
x0 = zeros(d,1);

% Form Ne evaluation Halton points
xe_temp = halton(Ne, d);

% Tolerance for the criterion
TOL = 1e-2;

% Possible values of t
tvec = linspace(0.3, 0.99, 10); 

% Initialize error arrays
error_h = zeros(numel(epvec), numel(Nvec));
error_g = zeros(numel(epvec), numel(Nvec));
error_d = zeros(numel(epvec), numel(Nvec));

for N_it = 1:numel(Nvec) % loop through the values of N
    % Set the current N
    N = Nvec(N_it);
    
    % Form Halton evaluation points. Exclude the first N points to not count the
    % collocation points
    xe = xe_temp((N+1):end, :);
    
    % Scale to the domain [-1, 1]^d
    xe = 2*xe - 1;
    clear xe_temp
    
    for ep_it=1:numel(epvec) % loop through the values of epsilon 

        % The closest cumulative degree K of polynomials to N
        K = compute_K(N, d);
        xk = halton(N, d);
        xk = 2*xk - 1;
        
        % Evaluate the function on the evaluation points (for test purposes)
        fvals = f(xe);
        compute_error = @(interpolant) compute_avg_error(interpolant, fvals);
        
        % Set current value of epsilon
        ep = epvec(ep_it);
        
        % Form matricies E and G
        E = ep.*Pe;
        E = E.'*E;
        
        % Evaluate the inteprolated function at collocation points
        y = f(xk);
        
        % Compute the HermiteGF-QR interpolant
        s = compute_QR_interpolant(E, G, xk, xe, K, maxlength, y, tvec, TOL);
        
        % Recompute the number of the evaluation points since we excluded some.
        Ne = size(xe, 1);
        
        % Compute the error
        error_h(ep_it, N_it) = compute_error(s);
        
        if benchmark_gqr % if we want to compare to the result of the GaussQR method
            if (exist('gaussqr') == 7)
                % Set the maximum number of basis functions to infinity, so
                % that we take all basis functions that impact the quality
                % of the solution
                GAUSSQR_PARAMETERS.MAX_EXTRA_EFUNC = Inf;
                
                % Compute the GaussQR interpolant
                GQR = gqr_solve(xk,y,ep,gammaval);
                yp = gqr_eval(GQR,xe);
                
                % Compute the observables
                error_g(ep_it, N_it) = compute_error(yp); 
            else
                fprintf('Please download the gaussQR library and run "rbfsetup.m" in its main folder in order to use the "benchmark_gaussQR" option.\n');
                return
            end
        end
        
        % If you chose to bencmark against the direct solution
        if benchmark_direct
            % Compute the direct interpolation matrix
            [cond_A0, interpolation_matrix_d] = compute_interp_mtx_direct(ep, xk, xe, Pe);
            
            % Compute the direct interpolant
            s_d = interpolation_matrix_d*f(xk);
            
            % Compute the error
            error_d(ep_it, N_it) = compute_error(s_d);
        end
        
    end
end

% Figure window setup
x0=10;
y0=10;
height = 400;
width=500;

% Plotting of the results for all test cases
switch(test)
    case 'ndtest'
        figure(1)
        leg1 = legend;
        l1 = leg1.String;
        loglog(epvec, error_h, '-','Linewidth', 1.5, 'Color', [0 0.7 0], 'Linewidth', 1.5, 'Marker', '<', 'MarkerSize', 9,'MarkerIndices', 2:4:length(error_h))%[0.7 0 0])
        hold on
        l1{end+1} = strcat('HGFQR (', num2str(d), 'D)');
        minval = min(error_h);
        maxval = max(error_h);
        if (benchmark_gqr)
            loglog(epvec, error_g, '--', 'Linewidth', 1.5, 'Marker', 's', 'MarkerSize', 9, 'Color', [0 0.7 0], 'MarkerIndices', 4:4:length(error_g))
            l1{end+1} = strcat('GQR (', num2str(d), 'D)');
            minval = min([minval, error_g']);
            maxval = max([maxval, error_g']);
        end
        axis([min(epvec) max(epvec) 0.8*minval 2*maxval])
        set(gcf,'units','points','position',[x0,y0,width,height])
        legend(l1, 'Interpreter', 'Latex', 'Fontsize', 18);
        set(gca, 'Fontsize', 18)
        xlabel('$\varepsilon$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('Average error', 'Interpreter', 'Latex', 'Fontsize', 18)
        title(strcat('$\gamma = ', num2str(gammaval), '$'), 'Interpreter', 'Latex', 'Fontsize', 18)
    case '3dtest'
        figure(2)
        leg1 = legend;
        l1 = leg1.String;
        loglog(epvec, error_h, '-','Linewidth', 1.5, 'Color', [0 0 0.7], 'Linewidth', 1.5, 'Marker', '<', 'MarkerSize', 9,'MarkerIndices', 1:4:length(error_h))%[0.7 0 0])
        hold on
        l1{end+1} = strcat('HGFQR, $N=', num2str(Nvec), '$');
        minval = min(error_h);
        maxval = max(error_h);
        if (benchmark_direct)
            loglog(epvec, error_d, '--', 'Linewidth', 1.5, 'Marker', 'pentagram', 'MarkerSize', 9, 'Color', [0 0 0.7], 'MarkerIndices', 3:4:length(error_d));
            l1{end+1} = strcat('Direct, $N=', num2str(Nvec), '$');
            minval = min([minval error_d']);
            maxval = max([maxval error_d']);
        end
        axis([min(epvec) max(epvec) 0.8*minval 2*maxval])
        set(gcf,'units','points','position',[x0,y0,width,height])
        title(strcat('$\gamma = ', num2str(gammaval), '$'), 'Interpreter', 'Latex', 'Fontsize', 18)
        legend(l1, 'Interpreter', 'Latex', 'Fontsize', 18);
        xlabel('$\varepsilon$', 'Interpreter', 'Latex', 'Fontsize', 18)
        ylabel('Average error', 'Interpreter', 'Latex', 'Fontsize', 18)
        set(gca, 'Fontsize', 18)
end
