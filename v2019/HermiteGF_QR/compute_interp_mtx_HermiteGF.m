function [interp_mtx, cond_A0] = compute_interp_mtx_HermiteGF(E, G, xk, xe, j, t, factorial_part1)
%% Computes the d-dimensional HermiteGF-QR inteprolation matrix with a zero shift x0 = 0.
%
% Arguments:
% E - shape matrix
% G - G matrix
% xk - collocation points
% xe - evaluation points
% j - (M \times d) array of indexes of basis functions in corresponding dimensions
% t - the value of the parameter t
% factorial_part1 - an array containing the values of factorials required
% for the first N \times N block of the matrix D
%
% OUTPUT:
% interp_mtx - interpolation matrix
% cond_A0 - conditioning of the collocation matrix

%%
M = size(j, 1);
N = size(xk, 1);

% Form G^{-1}E^T E
G_inv = inv(G);
GinvE = G\E;
d = size(E, 1);

% Form the Diag matrix
diagonal = ones(d, 1); % This just creates a placeholder for diagonal elements
% so that the diag() function of MATLAB returns it in a proper format
diagonal = diag(GinvE);
Diag = diag(diagonal);

% The Diag^{-1}*Rem matrix
corr = (Diag^(-1))*(GinvE - Diag);

% Compute the matrix C
C = compute_C(E, xk, j, G_inv, corr);
[Q, R] = qr(C);

clear Q C
% Compute \bar{R}
Rt = R(1:N, 1:N) \ R(1:N, N+1:M);

if ( M > N ) % if there is a part to scale
    D = compute_effect_D(diagonal, N, N+1, M, j, t, factorial_part1);
    Rt = D.*Rt;
end

% Compute collocation matrix
He = evaluate_HermiteGF_basis(xk, E, G, j, t);
A0 = He(:, 1:N) + He(:, (N + 1):M) * Rt.';

clear He
% Compute evaluation matrix
He_eval = evaluate_HermiteGF_basis(xe, E, G, j, t);
A = He_eval(:, 1:N) + He_eval(:, (N + 1):M) * Rt.';

clear He_eval Rt
% Compute interpolation matrix
interp_mtx = A/A0;

% For testing purposes also return the conditioning of the
% collocation matrix
cond_A0 = cond(A0);
end