function jmax = compute_jmax(E, G, K, xk, t, jtemp, factorialPart1, TOL, maxlength)
%% Compute the maximum degree of the polynomials of the expansion functions jmax
%
% Arguments:
% E - shape matrix
% G - G matrix
% K - the highest polynomial degree corresponding to the number of
% collocation points N
% xk - collocation points
% t - parameter t
% jtemp - index array up to degree K
% factorial_part1 - an array containing the values of factorials required
% for the first N \times N block of the matrix D
% TOL - tolerance for the cut-off criterion
% maxlength - maximum allowed number of basis functions
%
% OUTPUT
% jmax - cut-off degree for the HermiteGF expansion 
%
%%
N = size(xk, 1);

% Dimensionality
d = size(E, 1);

% Form G^{-1}E^T E
GinvE = G\E;

% Form the Diag matrix
diagonal = ones(d, 1); % This just creates a placeholder for diagonal elements
% so that the diag() function of MATLAB returns it in a proper format
diagonal = diag(GinvE);
Diag = diag(diagonal);
d = numel(diagonal); % dimensionality

% The Diag^{-1}*Rem matrix
corr = (Diag^(-1))*(GinvE - Diag);

% Transpose xk for convenience
xk = xk';
yk = GinvE*xk;

% Scale the diagonal with sqrt(2/t)
diagonal_t_2 = diagonal.*(sqrt(2/t));
% We start with the jmax = K as it is the minimum possible value for the QR
% method
jmax = K;
M = size(jtemp, 1);

% Compute the squared norm of yk scaled with the diagnoal^(1/d)
norm_yk_temp = zeros(1, N);
yk_temp = yk./(prod(diagonal)^(1/d));
for i = 1:N
    norm_yk_temp(i) = sum((yk_temp(:, i)).^2);
end

% Compute the squared norm of yk
norm_yk = zeros(1, N);
for i = 1:numel(norm_yk)
    norm_yk(i) = sum((yk(:, i)).^2);
end

% Compute exp(\Vert yk \Vert_2^2)
exp_norm_yk = exp((2/t).*norm_yk);

% Compute (diagonal_t_2^(-|k| + (jmax+1)/d)) for all k
diag_pow_2k = zeros(1, N);
for k = 1:N
    diag_pow_2k(k) = prod(diagonal_t_2.^(2.*(- jtemp(k,:)' + ((jmax+1)/d).*ones(numel(diagonal), 1))));
end

%%%%% !!!!!!!!!!!!!!! WAS CHANGED TO FACTORIALPART1 !!!!!!!!!!!!!!!!
% Compute the factorial of the indexes up to id=N
% factorial_k = zeros(1, N);
% for k = 1:N
%     factorial_k(k) = prod(factorial(jtemp(k,:)));
% end

% norm(factorial_k - factorialPart1')
% Evaluate the first N basis functions
He = evaluate_HermiteGF_basis(xk', E, G, jtemp, t);

% Form the matrices W_1 and W_2
W = compute_W(xk', jtemp, corr, 1:size(jtemp, 1));
W1 = W(1:N, 1:N);
W_inv = pinv(W1);
W2 = W(:, (N+1):size(jtemp, 1));
clear W W1;

% Compute the basis \Psi
D = compute_effect_D(diagonal, N, N+1, M, jtemp, t, factorialPart1);
Rt = D.*(W_inv*W2);
Psi = He(:, 1:N) + He(:, (N + 1):M) * Rt.';

% Compute the coefficients \omega_k
for k=1:N
    omega(k) = sum(W_inv(k,:).^2);
end

% Compute the limit values for the ||H(\x_k)||_2^2 for all collocation
% points
HeLimit = zeros(N, 1);
for k=1:N % loop through grid points
    x_temp = G'*xk(:,k);
    HeLimit(k) = (exp(-2*(xk(:,k)')*E*xk(:, k))/(1-t^2))*exp((2*t*(x_temp'*x_temp))/(1+t));
end

% Computing the criterion expression. First, we compute
% \sum \exp(||\tilde{y}_i||_2^2)||x_i||_2^(2\jmax + 1)
y_part = 0;
for i = 1:N
    y_part = y_part + exp_norm_yk(i)*(norm_yk_temp(i)^(jmax+1));
end

% Computing the second sum
dvec_part = 0;
for k = 1:N
    dvec_part = dvec_part + omega(k)*(factorialPart1(k)/factorial(jmax+1))*diag_pow_2k(k); %*((t/2)^(- jmax - 1 + sum(jtemp(k,:))))*diag_pow_2k(k);
end

% Compute the sum of squares of the first N basis functions evaluated at
% all collocation points
for k = 1:N
    sumHe(k) = sum(He(k, :).^2);
end
% Combining everything together, to get the value of the criterion in all
% collocation points
for k = 1:N
    crit(k) = dvec_part*y_part*abs(HeLimit(k) - sumHe(k))/(sum(Psi(k, :).^2));
end
clear He D W2
while (max(sqrt(crit))) > TOL && (size(jtemp,1)  < maxlength) % && (jmax < current_jmax)
    % Increase the degree by one
    jmax = jmax+1;
    
    % Compute (diagonal_t_2^(-|k| + (jmax+1)/d))
    diag_pow_2k = zeros(1, N);
    for k = 1:N
        diag_pow_2k(k) = prod(diagonal_t_2.^(2.*(- jtemp(k,:)' + ((jmax+1)/d).*ones(numel(diagonal), 1))));
    end
    
    % Add the indexes for the new degree to the index array
    jtemp = generate_index(jtemp', jmax, maxlength, d)';
    
    % Find the indexes of the next level of basis functions
    x = find(sum(jtemp,2)==jmax);
    
    % Compute the additional block of the matrix W
    W2add = compute_W(xk', jtemp, corr, x);
    
    % Compute the index range of the added basis functions
    i2min = numel(find(sum(jtemp, 2) <= jmax - 1))+1;
    i2max = size(jtemp, 1);
    
    % Compute the block of Dtilde corresponding to the added basis functions
    Dadd = compute_effect_D(diagonal, N, i2min, i2max, jtemp, t, factorialPart1);
    
    % Evaluate the newly added basis functions
    HePart = evaluate_HermiteGF_basis_part(xk', E, G, jtemp, t, i2min, i2max);
    
    % Compute the stable basis
    Psi = Psi + HePart*(Dadd.*(W_inv*W2add))';
    
    % Computing the criterion expression. First, we compute
    % \sum \exp(||\tilde{y}_i||_2^2)||x_i||_2^(2\jmax + 1)
    y_part = 0;
    for i = 1:N
        y_part = y_part + exp_norm_yk(i)*(norm_yk_temp(i)^(jmax+1));
    end
    
    % Compute the second sum
    dvec_part = 0;
    factorial_jmax = factorial(jmax+1);
    for k = 1:N
        dvec_part = dvec_part + omega(k)*(factorialPart1(k)/factorial_jmax)*diag_pow_2k(k);
    end
    
    % Add the Frobenius norm of the new block to the sum
    for k = 1:N
        sumHe(k) = sumHe(k) + sum(HePart(k, :).^2);
    end
    
    % Compute the criterion on all collocation points
    for k = 1:N
        crit(k) = dvec_part*y_part*abs(HeLimit(k) - sumHe(k))/(sum(Psi(k, :).^2));
    end
end
end
