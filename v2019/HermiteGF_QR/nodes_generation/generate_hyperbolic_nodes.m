function xk_new = generate_hyperbolic_nodes(N)
%% Generate hyperbolic nodes. Corresponds to Fig. 3
%
% Arguments:
% N - number of nodes
%
% OUTPUT:
% xk_new - N x 2 array of the hyperbolic nodes
%%
% Generate halton points and scale them to the interval [-1, 1] x [-1, 1]
xk = halton(N, 2);
xk = 2*xk-1;

% Cluster towards the boundaries
xk = sin(pi*xk./2); % xk(:,1) corresponds to c, xk(:, 2) corresponds to s
xk(:, 1) = xk(:,1)*0.4 + 0.6; % shift the values of c to [0.2, 1]

% Parametrization functions
cosh = @(u) (exp(u) + exp(-u))/2;
sinh = @(u) (exp(u) - exp(-u))/2;

xk_new = zeros(size(xk));
for i=1:N %loop through the points
    c = xk(i, 1);
    
    % Compute the t max for which the positive branch of the hyperbola 
    % intersects with the boundary circle on the right
    max_t = abs(log(2.2/c + sqrt((2.2)^2/(c.^2) - 1)));
    max_t = fzero(@(t) (sqrt((c*cosh(t) - 1.2)^2 + (c^2)*sinh(t)^2/4) - 1), max_t);
    
    % Scale the t with respect to max_t
    t = xk(i, 2)*max_t;
    
    % Compute the coordinates of the i-th point
    xk_new(i,:) = [c*cosh(t), 0.5*c*sinh(t)];
    
end
xk_new(:,1) = xk_new(:,1) - 1.2;
end