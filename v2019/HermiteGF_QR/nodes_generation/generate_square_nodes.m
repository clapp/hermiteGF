function xk = generate_square_nodes(nodes_type, clustering, N)
%% Generate unit square nodes clustered toward the boundaries
%
% Arguments:
% nodes_type - the type of nodes. Can be either 'Halton' or 'Uniform'
% clustering - TRUE or FALSE. Determines whether the clustering of the
% nodes toward the boundaries should be performed
% N - the amount of nodes
%
% OUTPUT:
% xk - N x 2 array of square nodes
%%
switch (nodes_type)
    case 'Halton' % generate Hatlon nodes
        if (numel(N) ~= 1)
            disp('For Halton points please enter the overall number of points N');
        end
        xk = halton(N, 2);
        xk = 2*xk - 1; % scale the nodes to the interval [-1, 1] \times [-1, 1]
        if (clustering) % cluster the nodes toward the boundaries
            xk = sin(pi.*xk/2);
        end
    case 'Uniform'
        if (numel(N) ~= 2)
            disp('For the tensor grid please enter the number of points in each direction N = [N_x, N_y]');
        end
        if (clustering)
            disp('Uniform grid is typically used for testing. Ignoring the "clustering" argument.');
        end
        Ne1 = N(1);
        Ne2 = N(2);
        xe = linspace(-1, 1, Ne1)'; % grid in x direction
        ye = linspace(-1, 1, Ne2)'; % grid in y direction
        [xx_e, yy_e] = ndgrid(xe, ye); % tensor-product grid
        xk = [xx_e(:) yy_e(:)];
end
end
