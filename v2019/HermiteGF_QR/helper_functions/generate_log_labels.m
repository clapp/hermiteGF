function [labels] = generate_log_labels(ticks)
for i = 1:numel(ticks)
    labels(i) = strcat("10^{", num2str(ticks(i)), "}");
end
end

