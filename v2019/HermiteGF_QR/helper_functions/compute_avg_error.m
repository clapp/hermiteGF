function error = compute_avg_error(interpolant, fvals)
%% Compute average error 
%
% Arguments:
% interpolant - the values of the interpolant
% fvals - the reference values
%
% OUTPUT:
% error - vector containing the average error
%
%%
Ne = size(fvals, 1);
error = sqrt(sum((((interpolant - fvals)./fvals).^2 )))./Ne;
end