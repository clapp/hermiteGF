function K = compute_K(N, d)
%% Compute the closest cumulative degree K of polynomials so that the full
% space of all polynomials up to degree K is of size at least N
%
% Arguments:
% N - space size
% d - dimensionality
%
% OUTPUT:
% K - the closest cumulative degree
%%
K = 0;
temp = factorial(K+d)/(factorial(d)*factorial(K));
while round(temp,0) < N
    K = K+1;
    temp = factorial(K+d)/(factorial(d)*factorial(K));
end
end