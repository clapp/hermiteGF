function j = generate_index(jold, jmax, maxlength, d)
%% This function has been borrowed from the Gauss-QR code developed by
% Gregory Fasshauer and Michael McCourt.
%
% The indices of the basis functions with a degree of the current level. At
% level 0 we have only one basis function [0 ... 0] which is the first basis
% function
%
% Arguments:
% jold - the current index array
% jmax - the desired maximum degree of the polynomials in the basis
% functions
% maxlength - maximum allowed amount of basis functions
% d - dimensionality
%
% OUTPUT:
% j - array containing the indexes of basis functions.
%%
if size(jold, 1) > 0
   jmaxOld = sum(jold(:, end));
   x = find(sum(jold,1)==jmaxOld);

   % The next degree to add
   Msum = jmaxOld + 1;
   j = jold; 
else
    x = [1];

    % The next degree to add
    Msum = 1;
    j = zeros(d,1);
end

% Loop while maximum degree or maximum amount of basis functions is reached
while Msum<=jmax && numel(j(1, :))<maxlength
    
    % Loop through all indexes with a degree Msum-1 and add 1 in all
    % dimensions to all indexes
    for k=x
        z = repmat(j(:,k),1,d)+eye(d);
        
        % Add only the indexes that are below maximal degree
        j = [j, z];
    end
    
    % Filter only unique indexes
    [b,m,n] = unique(j','rows');
    
    % Sort the array with respect to the polynomial degree
    j = j(:,sort(m));
    
    % Find the indexes of the next level of basis functions
    x = find(sum(j,1)==Msum);
    
    % Set the next level
    Msum = Msum + 1;
end
end