function [cond_A0, interp_mtx] = compute_interp_mtx_direct(ep, xk, xe, P)
%% Compute interpolation matrix for the direct anisotropic interpolation
%
% Arguments:
% ep - shape parameter
% xk - collocation points
% xe - evaluation points
% P - pattern for the shape matrix
%
% OUTPUT:
% cond_A0 - conditioning of the collocation matrix
% interp_mtx - interpolation matrix
%
%%
% Form the shape matrix E
E = ep.*P;
E = E'*E;

% Number of collocation points
N = size(xk, 1);

% Number of evaluation points
Ne = size(xe, 1);

% Initialize distance arrays
rk = zeros(N, N);
re = zeros(Ne, N);

% Evaluate anisortopic distance between collocation points
for i = 1:N
    for j = 1:N
        rk(i, j) = -(xk(i, :) - xk(j,:))*E*((xk(i, :) - xk(j,:)).');
    end
end

% Evaluate anisotropic distance between evaluation points
for i = 1:N
    for j = 1:Ne
        re(j, i) = -(xe(j, :) - xk(i,:))*E*((xe(j,:) - xk(i,:)).');
    end
end

% Evaluate the anisotropic basis
rbf = exp(rk);
rbf_eval = exp(re);

% Compute interpolation matrix
interp_mtx = rbf_eval/rbf;

% Save the conditioning of the collocation matrix
cond_A0 = cond(rbf);
end