function [ticks, labels] = set_log_ticklabels_colorbar(h)
ticks_old = get(h, 'Ticks');
ticks = [floor(ticks_old(1)) round(ticks_old(2:end-1)) ceil(ticks_old(end))];
ticks = unique(ticks);
labels = generate_log_labels(ticks);
set(h, 'Ticks', ticks, 'Ticklabels', labels)
end