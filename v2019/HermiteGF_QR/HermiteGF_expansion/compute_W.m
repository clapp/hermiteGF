function W = compute_W(xk, j, corr, x)
%% Compute a part of the Vandermonde matrix W
%
% Arguments:
% xk - collocation points
% j - index array
% corr - correction matrix Diag^{-1}*Rem
% x - the array containing the ids of the degrees of vk that have to be
% evaluated
%
% OUTPUT:
% W - the Vandermonde matrix W for points xk of the powers from j
%
%%
N = size(xk, 1);
M = numel(x);

% Initialize the matrix W
W = zeros(N, M);

% Compute the vk
vk = (xk' + corr*xk')';

% Get the ids corresponding to the multivariate indexes
ind = j(x, :);
for k = 1:N
    % Compute vk^j
    corr_term = prod(vk(k,:).^ind,2);
    W(k, :) = corr_term.';
end
end