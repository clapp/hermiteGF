function Dtilde_block = compute_effect_D(diagonal, N, i2min, i2max, j, t, factorialPart1)
% Compute the scaling effect of an additional block of the matrix D_1^{-1} and D_2 corresponding to the
% index range [i2min i2max] of newly added basis functions
%
% Arguments:
% diagonal - vector diag(Diag)
% N - number of collocation points
% i2min - the minimum id of the newly added basis functions
% i2max - the maximum id of the newly added basis functions
% t - parameter t
% factorialPart1 - an array containing the values of factorials required
% for the first N \times N block of the matrix D
%
% OUTPUT:
% Dtilde_block - the action of the block of \tilde{D} corresponding to the
% index range [i2min i2max]
% 
% Compute the factorials for the added block
factorialPart2inv = ones(i2max - i2min + 1, 1);
for i2 = i2min:i2max
    for d = 1:numel(diagonal)
          factorialPart2inv(i2 - i2min + 1) = factorialPart2inv(i2 - i2min + 1)/(prod(1:j(i2, d)));
    end
end

% Scale diagonal with the corresponding t factor
t2_sqrt= sqrt(2/t);
diagonal_t = diagonal.*t2_sqrt;

% First compute the factorial part
Dtilde_block = sqrt(factorialPart1*factorialPart2inv');

% Loop through dimensions 
for d = 1:numel(diagonal_t)
    % For each dimension compute the matrix containing all necessary powers
    % of the component diagonal_t(d) and multiply it with the existing
    % result
    Dtilde_block = Dtilde_block.*(diagonal_t(d).^((-repmat(j(1:N,d), [1 (i2max - i2min + 1)]) + repmat(j(i2min:i2max, d)', [N 1]))));
end