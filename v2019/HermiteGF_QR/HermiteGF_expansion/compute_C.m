function C = compute_C(E, xk, j, G_inv, corr)
%% Compute the coefficient matrix C up to degree jmax
%
% Arguments:
% E - shape matrix
% xk - collocation points
% j - index array
% G_inv - inverse of the G matrix
% corr - correction matrix Diag^{-1}*Rem
%
% OUTPUT
% C - the matrix C
%
%%
N = size(xk, 1);
M = size(j, 1);

% Initialize the matrix C
C = zeros(N, M);
vk = xk' + corr*xk'; % Compute the vector vk = xk + Diag^{-1}Rem xk
for k = 1:N % loop through vk points
    % Compute vk^j
    corr_term = prod(vk(:,k)'.^j, 2);
    % Multiply with the corresponding exponent
    C(k, :) = exp(-xk(k,:)*E*xk(k, :)' + xk(k,:)*(E')*(G_inv')*G_inv*E*xk(k,:)').*corr_term.';
end
end