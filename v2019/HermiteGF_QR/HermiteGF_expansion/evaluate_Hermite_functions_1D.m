function He = evaluate_Hermite_functions_1D(xk, N, t)
%% Compute N 1D Hermite functions scaled with \sqrt{t} on xk points. 
% The factor pi^{-1/4} is ignored since it would be cancelled out later on
% anyway
%
% Arguments:
% xk - points where the Hermite functions have to be evaluated
% N - number of basis functions
% t - parameter t
%
% OUTPUT:
% He - matrix containing the values of the first N 1D Hermite functions on the points
% xk
%
%%
% Initialize the result matrix
He = zeros(length(xk), N);

% Write the values of the first two Hermite functions to initialize the
% three-term recurrence
He(:, 1) = exp(-0.5.*((xk).^2));
He(:, 2) = xk.*sqrt(2).*exp(-0.5.*((xk).^2));

% Three term recurrence for the Hermite functions with argument gamma*x
for i = 3:N
    He(:, i) = sqrt(2.0/(i-1)).*xk.*He(:, i-1) - sqrt((i-2.0)/(i-1)).*He(:, i-2);
end

% Scale with \sqrt{t} in corresponding degrees
for i = 1:N
    He(:, i) = He(:, i).*(t^((i-1)/2));
end

end