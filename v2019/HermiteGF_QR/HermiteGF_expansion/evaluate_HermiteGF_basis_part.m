function He = evaluate_HermiteGF_basis_part(xe, E, G, j, t, i2min, i2max)
%% Evaluate the multivariate HermiteGF basis
%
% Arguments:
% xe - points on which the basis has to be evaluated
% E - shape matrix
% G - G matrix
% j - index array
% t - parameter t
% i2min - the minimum id of the newly added basis functions
% i2max - the maximum id of the newly added basis functions
%
% OUTPUT:
% He - matrix containing the values of the HermiteGF basis with the indexes
% from i2min:i2max lines of j, evaluated at points xe
%%
Ne = size(xe, 1);

% Dimensionality
d = size(E, 1);

% The points in which the Hermite polynomials should be evaluated
x_temp = (G'*xe')';

% Array containing the values of the polynomial part of the basis in all
% dimensions, stored separately for all dimensions
He_temp = zeros(Ne, max(max(j))+1, d);

for i = 1:d % loop through dimensions
    % Maximum polynomial degree in dimension i
    j_max = max(j(:, i));
    
    % Compute the 1D Hermite functions up to degree j_max (already scaled
    % with t)
    He_temp(:, 1:j_max+1, i) = evaluate_Hermite_functions_1D(x_temp(:, i), j_max + 1, t);
end
 
% Evaluate the multivariate scaled Hermite functions
He_f = ones(Ne, i2max - i2min + 1);
for j_ind = i2min:i2max
    for l = 1:d % for each basis functions compute the corresponding product of one-dimensional scaled Hermite functions
        He_f(:, j_ind - i2min + 1) = He_f(:, j_ind - i2min + 1).*He_temp(:, j(j_ind, l)+1, l);
    end
end

% Compute the argument of the exponential scaling required to get the HermiteGF
% basis
E_tilde = E - 0.5*(G*G');
% Scale with the multivariate exponential to get the HermiteGF basis functions
He = He_f.*repmat(exp(-sum((xe*E_tilde).*xe,2)), 1, i2max - i2min+1);

end