function He = evaluate_HermiteGF_basis(xe, E, G, j, t)
%% Evaluate the multivariate HermiteGF basis
%
% Arguments:
% xe - points on which the basis has to be evaluated
% E - shape matrix
% G - G matrix
% j - index array
% t - parameter t
%
% OUTPUT:
% He - matrix containing the values HermiteGF basis with indexes from j at
% points xe
%%
Ne = size(xe, 1);
M = size(j,1);

% Dimensionality
d = size(E, 1);

% The points in which the Hermite polynomials should be evaluated
x_temp = (G'*xe')';

% Array containing the values of the polynomial part of the basis in all
% dimensions, stored separately for all dimensions
He_temp = zeros(Ne, max(max(j))+1, d);

for i = 1:d % loop through dimensions
    % Maximum polynomial degree in dimension i
    j_max = max(j(:, i));
    
    % Compute the 1D Hermite functions up to degree j_max (already scaled
    % with t)
    He_temp(:, 1:j_max+1, i) = evaluate_Hermite_functions_1D(x_temp(:, i), j_max + 1, t);
end
 
% Evaluate the multivariate scaled Hermite functions
He_f = ones(Ne, M);
for j_ind = 1:M % for each basis functions compute the corresponding product of one-dimensional scaled Hermite functions
    for l = 1:d
        He_f(:, j_ind) = He_f(:, j_ind).*He_temp(:, j(j_ind, l)+1, l);
    end
end
clear j

% Compute the argument of the exponential scaling required to get the HermiteGF
% basis
E_tilde = E - 0.5*(G*G');
% Scale with the multivariate exponential to get the HermiteGF basis functions
He = He_f.*repmat(exp(-sum((xe*E_tilde).*xe,2)),1,M);

end