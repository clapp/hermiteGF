function [s, cond_A0, jmax] = compute_QR_interpolant(E, G,  xk, xe, K, maxlength, y, tvec, TOL)
%% Computes the HermiteGF-QR interpolant with a zero shift x0 = 0.
%
% Arguments:
% E - shape matrix
% G - G matrix
% xk - collocation points
% xe - evaluation points
% K - the highest polynomial degree corresponding to the number of
% collocation points N
% maxlenth - maximum allowed number of basis functions
% y - the values of the interpolant in collocation points
% tvec - array of possible values of t
% TOL - tolerance for the cut-off criterion
%
% OUTPUT:
% s - interpolant
% cond_A0 - conditioning of the collocation matrix
% jmax - cut-off degree of the HermiteGF expansion
%%
% Determine the dimensionality of the problem
dim = size(xk, 2);
N = size(xk, 1);

% Generate the index array for the first K polynomial degrees
jtemp = generate_index([], K, maxlength, dim)';

% Precompute the factorials of the D_1 (N x N part of D)
factorialPart1 = ones(N, 1);
for i = 1:N
    for d = 1:dim
        factorialPart1(i) = factorialPart1(i)*prod(1:jtemp(i,d));
    end
end

% Determine the number of basis functions jmax and create the
% corresponding index array j
jmaxvec = zeros(size(tvec));
for t_it = 1:numel(tvec)
    t = tvec(t_it);
    jmaxvec(t_it) = compute_jmax(E, G, K, xk, t, jtemp, factorialPart1, TOL, maxlength);
end

% Determine t corresponding to the minimum jmax
jmax = min(jmaxvec);
ind = find(jmaxvec == jmax);
t = tvec(ind(end));

j = generate_index([], jmax, maxlength, dim)';

% Compute the interpolation matrix
[interpolation_matrix, cond_A0] = compute_interp_mtx_HermiteGF(E, G, xk, xe, j, t, factorialPart1);

% Compute the interpolant
s = interpolation_matrix*y;

end