MATLAB HermiteGF stabilization code for the stable isotropic and anisotropic Gaussian RBF interpolation, version of 2019.

Author: Anna Yurova

This is a MATLAB implementation of the method described in the paper

"Stable Interpolation with isotropic and anisotropic Gaussians using Hermite generating function"

by Katharina Kormann, Caroline Lasser and Anna Yurova.

This is an enhancement of the older version v2017. In particular, we provide the implementation of the HermiteGF-QR method for anisotropic Gaussians in arbitrary dimension. The the implementation of the method itself is located in the folder "HermiteGF_QR". The tests, that are also presented in the paper, are located in the folder "tests". 
