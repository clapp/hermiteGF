HermiteGF stabilization code for the RBF interpolation. 
Author: Anna Yurova

This is an implementation of the method described in the paper

"STABLE EVALUATION OF GAUSSIAN RADIAL BASIS FUNCTIONS USING HERMITE POLYNOMIALS"

by Anna Yurova and Katharina Kormann.

https://arxiv.org/abs/1709.02164

Functionality (MATLAB):
- HermiteGF-tensor interpolation in 1-2D (running scripts: MATLAB/tensor/hermiteGF_1D.m, MATLAB/tensor/hermiteGF_tensor_2D.m).
- HermiteGF-QR in 1D (running script: MATLAB/QR/hermiteGF_QR.m).
- anisotropic interpolation with full grid of basis functions in 2D (running script: MATLAB/anisotropic_hermite_2D/interpolate_anisotropic.m).

Functionality (Julia):
- HermiteGF-tensor in 1-5D. The implementation of 4-5D cases is parallel. 5D tests have to be run on a cluster to be finished in a reasonable time.
