%% The BSD license
% Copyright (c) 2017  Anna Yurova, Max-Planck-Institut für Plasmaphysik. All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of the Max-Planck-Institut f\"ur Plasmaphysik nor the
%       names of its contributors may be used to endorse or promote products
%       derived from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
% INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL MAX-PLANCK-INSTITUT FÜR PLASMAPHYSIK BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%
%
% In addition, we ask you to cite the following reference in
% scientific publications which contain results obtained with
% this software and developments:
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%

%% Description
% Author: Anna Yurova
% This is an example code for the stable 1D RBF interpolation using HermiteGF
% basis functions. The algorithm is based
% on the generating functions theory and described in
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%
% We kindly ask you to to cite the reference to the above mentioned paper
% when using the code.

%% Source code
clear

% Interpolated function

%--------- f_1 from the paper ---------
fun = @(x) exp(x).*sin(2*pi*x) + (1./(1+x.^2));
function_name = 'f1';
L = 1; % radius of the interpolation interval
gamma = 2;
%--------------------------------------

%--------- f_2 from the paper ---------
% fun = @(x) sin(x/2) - 2*cos(x) + 4*sin(pi*x); % f_2 from the paper
% function_name = 'f2';
% L = 4; % radius of the interpolation interval
% gamma = 1;
%--------------------------------------

%--------- f_4^c from the paper -------
% fun = @(x) cos(4*x.^2);
% function_name = 'cos4x2';
% L = 1; % radius of the interpolation interval
% gamma = 3;
%--------------------------------------

% Number of basis functions (the same as the number of expansion functions)
Nvec = [20 25 30];

% Number of evaluation points
Ne = 100;

% Type of nodes
nodes_name  =  'Chebyshev'; %'Uniform';%

% Shape parameter
epvec = 0.01:0.001:2;

% Generating the evaluation points
xe = linspace(-L, L, Ne)';

% Allocating vectors of errors
max_abs_error_hermite = NaN(size(epvec));
L2_error_hermite = NaN(size(epvec));

% Colors for different values of N
colors = {[0 0 0.7], [0 0.7 0], [0.7 0 0]};

% Loop through the values of N
for N_iter = 1:numel(Nvec)
    N = Nvec(N_iter);
    
    % Generate node points
    if (strcmp(nodes_name, 'Chebyshev'))
        theta = (pi:(-pi / (N - 1)):0)';
        xk = L*cos(theta);
    elseif (strcmp(nodes_name, 'Uniform'))
        xk = L*linspace(-1, 1, N)';
    end
    
    % Evaluate the function at the collocation points
    f = fun(xk);
    
    % Loop through the values of the shape parameter
    for i = 1:numel(epvec)
        
        % Choose the value of the shape parameter
        ep = epvec(i);
        
        %----------------------- HermiteGF method -------------------------
        
        % Evaluate the HermiteGF basis functions on the collocation points
        He = evaluate_hermite(xk, N, ep, gamma);
        
        % Evaluate the HermiteGF basis functions on the evaluation points
        He_eval = evaluate_hermite(xe, N, ep, gamma);
        
        % Compute the interpolantion matrix
        
        interpolation_matrix = (He_eval/He);
        
        % Compute the interpolated values
        s = interpolation_matrix*f;
        
        % Evaluate the original function at evaluation points to compare with the
        % interpolated one
        f_vals = fun(xe);
        
        % Compute errors
        max_abs_error_hermite(i) = max(abs(s - f_vals));
        L2_error_hermite(i) = sqrt(trapz(xe, (s - f_vals).^2));
        %----------------------------------------------------------------
    end
    
    % Plot maximum error
    %     loglog(epvec, max_abs_error_hermite, 'Color', colors{N_iter}, 'LineWidth', 2);
    %     hold on
    
    % Plot L2 error
    loglog(epvec, L2_error_hermite, 'Color', colors{N_iter}, 'LineWidth', 2);
    hold on
end
