%% The BSD license
% Copyright (c) 2017  Anna Yurova, Max-Planck-Institut für Plasmaphysik. All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of the Max-Planck-Institut f\"ur Plasmaphysik nor the
%       names of its contributors may be used to endorse or promote products
%       derived from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
% INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL MAX-PLANCK-INSTITUT FÜR PLASMAPHYSIK BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%
% In addition, we ask you to cite the following reference in
% scientific publications which contain results obtained with
% this software and developments:
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%

%% Description
% Author: Anna Yurova
% This is an example code for the stable 2D RBF interpolation using HermiteGF
% basis functions. The algorithm is based
% on the generating functions theory and the underlying tensor product structure.
% The algorithm is described in
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%
% We kindly ask you to to cite the reference to the above mentioned paper
% when using the code.

%% Source code
clear

% Interpolated function

%--------- f_3 from the paper -------
fun = @(x, y) cos(x.^2 + y.^2);
Lx = 1;
Ly = 1;
gamma1 = 3;
gamma2 = 3;
%--------------------------------------

% Number of evaluation points
Ne1 = 53; % x direction
Ne2 = 53; % y direction

% Type of nodes
nodes_name  =  'Chebyshev'; %'Uniform'; %

% Shape parameter
ep1 = 0.1; % x direction
ep2 = 0.1; % y direction

% Generating the evaluation points
xe = Lx*linspace(-1, 1, Ne1)'; % x direction
ye = Ly*linspace(-1, 1, Ne2)'; % y direction
[xx_e, yy_e] = ndgrid(xe, ye);

% Number of basis functions (the same as the number of expansion functions)
Nvalues = 5:35;

% Allocating vectors of errors
Hermite_L2_error = NaN(1, numel(Nvalues));
Hermite_max_error = NaN(1, numel(Nvalues));

for N_iter = 1:numel(Nvalues)
    
    % Specify number of basis functions (also collocation points)
    Nx = Nvalues(N_iter); % x direction
    Ny = Nx; % for testing purposes taking the same as Nx, however, can be different
    
    % Generate node points
    if (strcmp(nodes_name, 'Chebyshev'))
        theta1 = (pi:(-pi / (Nx - 1)):0)';
        xk = Lx*cos(theta1);
        theta2 = (pi:(-pi / (Ny - 1)):0)';
        yk = Ly*cos(theta2);
    elseif (strcmp(nodes_name, 'Uniform'))
        xk = Lx*linspace(-1, 1, Nx)';
        yk = Lx*linspace(-1, 1, Ny)';
    end
        
    % Form the 2D collocation grid
    [xx_k, yy_k] = ndgrid(xk, yk);
    
    % Evaluate the function at the 2D collocation grid
    f = fun(xx_k, yy_k);
    
    % Compute 1D collocation matrices
    X_collocation = evaluate_hermite(xk, Nx, ep1, gamma1); % in x direction
    Y_collocation = evaluate_hermite(yk, Ny, ep2, gamma2); % in y direction
    
    % Compute 1D evaluation matrices
    X_evaluation = evaluate_hermite(xe, Nx, ep1, gamma1); % in x direction
    Y_evaluation = evaluate_hermite(ye, Ny, ep2, gamma2); % in y direction
    
    % Compute 1D interpolantion matrices
    X_all = X_evaluation/X_collocation;
    Y_all = Y_evaluation/Y_collocation;
    
    % Compute the interpolated values
    s = X_all*f*Y_all';
    
    % Evaluate the original function at evaluation points to compare with the
    % interpolated one
    f_vals = fun(xx_e, yy_e);
    
    % Compute errors
    Hermite_L2_error(N_iter) = sqrt(trapz(xe, trapz(ye, (s - f_vals).^2, 2)));
    Hermite_max_error(N_iter) = max(abs(s(:) - f_vals(:)));
end

semilogy(Nvalues, Hermite_L2_error, 'Color', [0 0.7 0], 'Marker', '^', 'Linewidth', 2)
