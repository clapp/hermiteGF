%% The BSD license
% Copyright (c) 2017 Anna Yurova, Max-Planck-Institut für Plasmaphysik. All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of the Max-Planck-Institut f\"ur Plasmaphysik nor the
%       names of its contributors may be used to endorse or promote products
%       derived from this software without specific prior written permission.
%
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
% INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL MAX-PLANCK-INSTITUT FÜR PLASMAPHYSIK BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%
% In addition, we ask you to cite the following reference in
% scientific publications which contain results obtained with
% this software and developments:
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%

%% Description 
% Author: Anna Yurova
% This is an example code for the stable 1D RBF interpolation using HermiteGF-QR method. 
% The algorithm is based on the generating functions theory in combination with the ideas of
% RBF-QR approach. The algorithm is described in
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%
% We kindly ask you to to cite the reference to the above mentioned paper
% when using the code.
%
% The code was largely inspired by the existing RBF-QR code that can be
% found at http://www.it.uu.se/research/scientific_computing/software/rbf_qr with
% the following terms of use:
% "Terms of use
% These subroutines may be used freely without permission, but not for  
% commercial purposes.
% When used for research publications, we kindly ask users to cite and
% acknowledge the source."

%% Source code
clear

% Interpolated function

%--------- f_2 from the paper ---------
fun = @(x) sin(x / 2) - 2 * cos(x) + 4 * sin(pi * x);
L = 4;
function_name = 'f2';
gamma = 1;
%--------------------------------------

%--------- f_1 from the paper ---------
% fun = @(x) exp(x) .* sin(2 * pi * x) + (1 ./ (x.^2 + 1));
% stretch_factor = 1;
% function_name = 'f1';
% gamma = 2;
%--------------------------------------

% The number of node points
Nvec = 20; % 10; % 20; % 25; %

% The number of HermiteGF basis functions
Mvec = Nvec + [0 1 2 3 4 5 30];

% Type of nodes
nodes_name = 'Chebyshev'; %'Uniform'; %

% The number of evaluation points
Ne = 100;

% Value(s) of the shape parameter
epvec = 0.1;

% Evaluation points
xe = linspace(-L,L,Ne)';

% 
L2_error_hermite = NaN(numel(Mvec), numel(Nvec), numel(epvec));
for N_iter = 1:numel(Nvec)
    for M_iter = 1:numel(Mvec)
        
        % Specify number of RBFs (also collocation points)
        N = Nvec(N_iter);
        
        % Specify the number of the HermiteGF expansion functions
        M = Mvec(M_iter);
        
        % Generate node points
        if (strcmp(nodes_name, 'Chebyshev'))
            theta = (pi:(-pi / (N - 1)):0)';
            xk = L * cos(theta);
        elseif (strcmp(nodes_name, 'Uniform'))
            xk = L * linspace(-1, 1, N)';
        end
        
        % Evaluate the function at the collocation points
        f = fun(xk);
        
        for k=1:length(epvec)
            clear A0 A interpolation_matrix
            epsilon = epvec(k);
            
            % Compute the coefficient matrix C, up to degree jmax
            C = ComputeC(epsilon, xk, M, L, gamma);
            
            % QR factorization of the matrix C
            [Q, R] = qr(C);
            
            % Compute R_1^{-1}*R_2
            Rt = R(1:N, 1:N) \ R(1:N, N+1:M);
            
            % If the correction D_2 is not empty
            if M > N
                D = computeEffectD(epsilon, N, M, L, gamma);  % Compute the effect of D_1^{-1} *...* D_2, 
                                                              %\tilde{D} from the paper        
                Rt = D .* Rt; % Apply the effect of \tilde{D} on R_1^{-1} R_2
            end
            
            % Evaluate the new basis at collocation points
            He = evaluate_hermite(xk, M, epsilon, gamma);
            
            % Compute the collocation matrix
            A0 = He(:, 1:N) + He(:, (N + 1):M) * Rt';
            
            % Evaluate the new basis at evaluation points
            He_eval = evaluate_hermite(xe, M, epsilon, gamma);
            
            % Compute the evaluation matrix
            A = He_eval(:, 1:N) + He_eval(:, (N + 1):M) * Rt';
            
            % Compute the interpolation matrix
            interpolation_matrix = A/A0;
            
            % Compute the interpolated values
            s = interpolation_matrix * f;

            % Evaluate the original function at evaluation points to compare with the
            % interpolated one
            f_vals = fun(xe);
        
            % Compute errors
            %max_abs_error_hermite(M_iter, N_iter, k) = max(abs(s - f_vals));
            L2_error_hermite(M_iter, N_iter, k) = sqrt(trapz(xe, (s - f_vals).^2));
        end
    end
end
%max_abs_error_hermite
L2_error_hermite
