%% The BSD license
% Copyright (c) 2017 Anna Yurova, Max-Planck-Institut für Plasmaphysik. All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of the Max-Planck-Institut f\"ur Plasmaphysik nor the
%       names of its contributors may be used to endorse or promote products
%       derived from this software without specific prior written permission.
%
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
% INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL MAX-PLANCK-INSTITUT FÜR PLASMAPHYSIK BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%
% In addition, we ask you to cite the following reference in
% scientific publications which contain results obtained with
% this software and developments:
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%

%% The function
function D_effect = computeEffectD(epsilon, N, M, L, gamma)
% This function returns a matrix that imitates the effect of D_1^-1 ...
% D^2 on a matrix X. After D_effect is computed, one has to element-wise multiply
% X.*D_effect in order to obtain the same effect as D_1^-1 * X * D_2
% The expressions of the elements of the matrix D are described in section
% 3.2 of the paper:
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%

% Initialize with NaNs
D_effect = NaN(N, M-N);
 for r = 1:N
     for c = 1:M-N
         % j1 = r-1; j2 = N + c - 1
         % each row we multiply with D_{j1 j1}^{-1} = (gamma^j1
         % sqrt(j1!))/(epsilon^{2j1}sqrt(2^j1) L^j2)
         % each column we multiply with D_{j2 j2} = (epsilon^{2j2}
         % sqrt(2^j1) L^j2) / (gamma^j2 sqrt(j2!)) 
         
         j1 = r - 1;
         j2 = N + c - 1;
         D_effect(r, c) = (epsilon^(2*(j2 - j1)))*(L^(j2 - j1))*(gamma^(j1 - j2))*sqrt(factorial(j1)/factorial(j2))*sqrt(2^(j2 - j1));
     end
 end
end