%% The BSD license
% Copyright (c) 2017 Anna Yurova, Max-Planck-Institut f\"ur Plasmaphysik. All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of the Max-Planck-Institut f\"ur Plasmaphysik nor the
%       names of its contributors may be used to endorse or promote products
%       derived from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
% INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL MAX-PLANCK-INSTITUT F\"UR PLASMAPHYSIK BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%
% In addition, we ask you to cite the following reference in
% scientific publications which contain results obtained with
% this software and developments:
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%
%% The function
function He = evaluate_hermite_symbolic_2D(xe, N, E, j)
% This function returns the matrix He of the values of the HagedornGF basis
% functions. The Hermite polynomials are evaluated
% using the built-in HermiteH function of the symbolic toolbox.
% This function should be used both for computation of the
% collocation and evaluation matrices.
%
% Parameters:
% xe - matrix of size (Ne, 2). Points in which Hermite polynomials should be evaluated
% N - number of basis functions to be evaluated
% E - shape matrix
% j - an array of size (N, 2). Contains the values of degrees of the
% polynomials in both directions


Ne = size(xe, 1);
% Initialize the resulting matrix with zeros
He = zeros(Ne, N);



% Determine the maximal degree of polynomials in each direction
jmax_x = max(j(:, 1));
jmax_y = max(j(:, 2));

% Setup the vectors for scaling the Hermite polynomials
factors_x = zeros(1, jmax_x+1);
factors_y = zeros(1, jmax_y+1);

factors_x(1) = 1;
factors_y(1) = 1;
% Fill in the vectors with polynomials scaling
for i = 1:jmax_x
    factors_x(i+1) =  sqrt(2^i * factorial(i));
end

for i = 1:jmax_y
    factors_y(i+1) =  sqrt(2^i * factorial(i));
end

% Compute the scaled Hermite polynomials in both directions

He_x = NaN(Ne, jmax_x+1);
for i = 1:Ne
    He_x(i, :) = hermiteH(0:jmax_x, xe(i, 1))./factors_x; % SCALING OF BASIS FUNCTIONS HAPPENS HERE
end

He_y = NaN(Ne, jmax_y+1);
for i = 1:Ne
    He_y(i, :) = hermiteH(0:jmax_y, xe(i,2))./factors_y; % SCALING OF BASIS FUNCTIONS HAPPENS HERE
end

% Evaluate HagedornGF basis functions
for i = 1:Ne
    for j_ind = 1:N
        He(i, j_ind) = He_x(i, j(j_ind, 1)+1)*He_y(i, j(j_ind, 2)+1);
        He(i, j_ind) = He(i, j_ind)*exp(-xe(i, :)*E*xe(i, :)');
    end
end

end