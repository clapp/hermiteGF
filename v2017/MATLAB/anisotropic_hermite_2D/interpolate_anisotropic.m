%% The BSD license
% Copyright (c) 2017 Anna Yurova, Max-Planck-Institut für Plasmaphysik. All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of the Max-Planck-Institut f\"ur Plasmaphysik nor the
%       names of its contributors may be used to endorse or promote products
%       derived from this software without specific prior written permission.
%
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
% INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL MAX-PLANCK-INSTITUT FÜR PLASMAPHYSIK BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%
%
% In addition, we ask you to cite the following reference in
% scientific publications which contain results obtained with
% this software and developments:
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%

%% Description
% This is an example code for the anisotropic interpolation of a 2D
% function using tensor product Hermite polynomials. The algorithm is based
% on generating functions theory and described in
%
% "STABLE EVALUATION OF GAUSSIAN RADIAL BASIS
%  FUNCTIONS USING HERMITE POLYNOMIALS"
%  by Anna Yurova and Katharina Kormann.
%
% We kindly ask you to to cite the reference to the above mentioned paper
% when using the code.

%% Source code
clear

% Interpolated function

%--------- f_a from the paper ---------
f = @(x, y)cos((x+y).^2./2.88 + (y-x).^2./4.5);

% Interpolation domain
xmin = -1;
xmax = 1;
ymin = -1;
ymax = 1;

% Evaluation domain
Ne1 = 17;
Ne2 = 17;
xe = linspace(xmin, xmax, Ne1)';
ye = linspace(ymin, ymax, Ne2)';
[xx_e, yy_e] = ndgrid(xe, ye);
evaluation_grid = [xx_e(:) yy_e(:)];
%--------------------------------------

% Maximum degree of polynomials in each direction
jmax = 10;

% Generating an array of powers for tensor product polynomials
[j_x, j_y] = meshgrid(1:jmax+1);
sum = j_x(:) + j_y(:);
[Y, ind] = sort(sum); % sorting the indices with respect to the cumulative degree
j = [j_x(:) - 1 j_y(:) - 1];
j = j(ind, :);
N = size(j, 1);
j = j(1:N, :); % all pairs of indices sorted via the cumulative power

% Generate node points
% Generate N halton nodes (by default generated in the domain [0,1] x [0,1]
xk = halton(N, 2);
% Shift the nodes to the interpolation domain [-1, 1] x [-1, 1]
xk = xk*2 - 1;

% Shape matrix
E = [0.16 0.04; 0.04 0.16];

% Evaluate tensor product Hermite polynomials and assemble collocation and
% evaluation matrices
interpolation_matrix = evaluate_hermite_symbolic_2D(xk, N, E, j);
evaluation_matrix = evaluate_hermite_symbolic_2D(evaluation_grid, N, E, j);

% Compute interpolation matrix
A = evaluation_matrix/interpolation_matrix;

% Compute the (vectorized) interpolated values
s = A*f(xk(:,1), xk(:,2));

% Reshape the interpolated values to 2D
interpolant = reshape(s, size(xx_e));

% Compute the errors
L2_error = sqrt(trapz(xe, trapz(ye, (interpolant - f(xx_e, yy_e)).^2, 2)))
max_error = max(abs(s - f(xx_e(:), yy_e(:))));
