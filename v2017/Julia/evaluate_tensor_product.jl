#=The BSD license:
——————————————
Copyright (c) 2017 Anna Yurova, Max-Planck-Institut für Plasmaphysik.
All rights reserved.
Redistribution and use in source and binary forms, with or without modifi-
cation, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
* Neither the name of the Max-Planck-Institut für Plasmaphysik nor the
names of its contributors may be used to endorse or promote products derived
from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ”AS
IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABIL-
ITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL MAX-PLANCK-INSTITUT FÜR PLASMAPHYSIK
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EX-
EMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLI-
GENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
——————————————
In addition, we ask you to cite the following reference in scientific publica-
tions which contain results obtained with this software and developments:
A. Yurova, K. Kormann
“Stable evaluation of Gaussian radial basis functions using Hermite polyno-
mials”
=#

###
# In this file the functions for the serial computing of the interpolated values 's'
# in 1-4D are implemented. Here the tensor representation of 's' is used (see Sec. 4.1 of the paper).
###


function evaluate_s_4D(X_all, Y_all, Z_all, W_all, F)

# Extract the number of collocation and evaluation points in each dimension
Nx = size(X_all, 2);
Ne1 = size(X_all, 1);
Ny = size(Y_all, 2);
Ne2 = size(Y_all, 1);
Nz = size(Z_all, 2);
Ne3 = size(Z_all, 1);
Nw = size(W_all, 2);
Ne4 = size(W_all, 1);

# Initialize the result tensor
s = zeros(Ne1, Ne2, Ne3, Ne4);

# Computing s = (W_all \otimes Z_all \otimes Y_all \otimes X_all) vec(F) (see Sec. 4.1 of the paper)
@inbounds for col_dim_4 = 1:Nw
  @inbounds for eval_dim_4 = 1:Ne4
    @inbounds for col_dim_3 = 1:Nz
      @inbounds for eval_dim_3 = 1:Ne3
        @inbounds for col_dim_2 = 1:Ny
          @inbounds for eval_dim_2 = 1:Ne2
            @inbounds for col_dim_1 = 1:Nx
              @inbounds for eval_dim_1 = 1:Ne1
                s[eval_dim_1, eval_dim_2, eval_dim_3, eval_dim_4] = s[eval_dim_1,eval_dim_2, eval_dim_3, eval_dim_4] + X_all[eval_dim_1, col_dim_1]*Y_all[eval_dim_2, col_dim_2]*Z_all[eval_dim_3, col_dim_3]*W_all[eval_dim_4, col_dim_4]*F[col_dim_1, col_dim_2, col_dim_3, col_dim_4];
              end
            end
          end
        end
      end
    end
  end
end
return s;
end

function evaluate_s_3D(X_all, Y_all, Z_all, F)

# Extract the number of collocation and evaluation points in each dimension
Nx = size(X_all, 2);
Ne1 = size(X_all, 1);
Ny = size(Y_all, 2);
Ne2 = size(Y_all, 1);
Nz = size(Z_all, 2);
Ne3 = size(Z_all, 1);

# Initialize the result tensor
s = zeros(Ne1, Ne2, Ne3);

# Computing s = (Z_all \otimes Y_all \otimes X_all) vec(F) (see Sec. 4.1 of the paper)
@inbounds for col_dim_3 = 1:Nz
  @inbounds for eval_dim_3 = 1:Ne3
    @inbounds for col_dim_2 = 1:Ny
      @inbounds for eval_dim_2 = 1:Ne2
        @inbounds for col_dim_1 = 1:Nx
          @inbounds for eval_dim_1 = 1:Ne1
            s[eval_dim_1, eval_dim_2, eval_dim_3] = s[eval_dim_1,eval_dim_2, eval_dim_3] + X_all[eval_dim_1, col_dim_1]*Y_all[eval_dim_2, col_dim_2]*Z_all[eval_dim_3 , col_dim_3]*F[col_dim_1, col_dim_2, col_dim_3];
          end
        end
      end
    end
  end
end
return s;
end

function evaluate_s_2D(X_all, Y_all, F)

  # Extract the number of collocation and evaluation points in each dimension
  Nx = size(X_all, 2);
  Ne1 = size(X_all, 1);
  Ny = size(Y_all, 2);
  Ne2 = size(Y_all, 1);

  # Initialize the result tensor
  s = zeros(Ne1, Ne2);

  # Computing s = (Y_all \otimes X_all) vec(F) (see Sec. 4.1 of the paper)
  @inbounds for col_dim_2 = 1:Ny
    @inbounds for eval_dim_2 = 1:Ne2
      @inbounds for col_dim_1 = 1:Nx
        @inbounds for eval_dim_1 = 1:Ne1
          s[eval_dim_1, eval_dim_2] = s[eval_dim_1,eval_dim_2] + X_all[eval_dim_1, col_dim_1]*Y_all[eval_dim_2, col_dim_2]*F[col_dim_1, col_dim_2];
        end
      end
    end
  end
  return s;
end

function evaluate_s_1D(X_all, F)

  # Extract the number of collocation and evaluation points
  Nx = size(X_all, 2);
  Ne1 = size(X_all, 1);

  # Initialize the result tensor
  s = zeros(Ne1);

  # Computing s =  X_all * vec(F) (see Sec. 4.1 of the paper)
  @inbounds for col_dim_1 = 1:Nx
    @inbounds for eval_dim_1 = 1:Ne1
      s[eval_dim_1] = s[eval_dim_1] + X_all[eval_dim_1, col_dim_1]*F[col_dim_1];
    end
  end

  return s;
end
