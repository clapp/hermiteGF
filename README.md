This is a repository containing two versions of the HermiteGF codes.

The first version is located in the folder "v2017" and corresponds to the preprint

"Stable evaluation of Gaussian radial basis functions using Hermite polynomials"

by Anna Yurova and Katharina Kormann.
https://arxiv.org/abs/1709.02164

The second (and latest version) is located in the folder "v2019" and corresponds to the submitted paper

"Stable Interpolation with isotropic and anisotropic Gaussians using Hermite generating function"

by Katharina Kormann, Caroline Lasser and Anna Yurova.

More detailed READMEs for both versions are available in the corresponding folders.